#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = "NETGEAR";
const char* password = "smilingmoon965";

ESP8266WebServer server(80);

class GPIOPIN {
    public:
        int pinNo;
        char * pinName;
        GPIOPIN(int no, char * name) {
            pinNo = no;
            pinName = name;
        }
};
GPIOPIN gpioPins[] = {
    GPIOPIN(1, (char *)"l1"),
    GPIOPIN(2, (char *)"l2"),
    GPIOPIN(3, (char *)"l3"),
    GPIOPIN(4, (char *)"l4"),
    GPIOPIN(5, (char *)"f1"),
    GPIOPIN(6, (char *)"f2"),
    GPIOPIN(7, (char *)"f3"),
    GPIOPIN(8, (char *)"f4"),
};

void handleRoot();
void handleNotFound();

void setup(void){
  for(int i = 0; i < sizeof(gpioPins)/sizeof(*gpioPins); i++) {
      pinMode(gpioPins[i].pinNo, OUTPUT);
      digitalWrite(gpioPins[i].pinNo, 0);
  }
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/on", handleRoot);
  server.on("/off", handleRoot);
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
}

void handleRoot() {
  digitalWrite(led, 1);
  server.send(200, "text/plain", "hello from esp8266!");
  digitalWrite(led, 0);
}

void handleNotFound(){
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}
