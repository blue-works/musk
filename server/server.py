from flask import Flask, render_template
from flask_socketio import SocketIO

app = Flask(__name__)
app.config['SECRET_KEY'] = 'SecretMusk'
socketio = SocketIO(app)


@app.route('/')
def index():
    return render_template('home.html')


@app.route('/on/')
def on():
    socketio.emit('message1', "on")
    return render_template('off.html')


@app.route('/off/')
def off():
    socketio.emit('message1', "off")
    return render_template('on.html')


if __name__ == '__main__':
    socketio.run(app)

print('Musk Running')